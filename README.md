**Java Facade**



*Wzorce projektowe*

---

## Fasada
Fasada upraszcza oraz unifikuje dostęp do grupy klas lub jednej klasy o rozbudowanym API. Fasada tworzy nowy interfejs o wysokiej abstrakcji, który upraszcza korzystania z całego systemu.

W praktyce z fasadą możemy spotkać się w sytuacji, gdy tworzymy serwis rozdzielony na frontend  oparty na technologiach webowych (Java Script, HTML, CSS, itd.) i backend gdzie komunikacja odbywa się przy pomocy zapytań RESTowych. Wtedy dobrze jest na wejściu backendu zastosować wzorzec Fasada, który dostarczy jedno zunifikowane API, a resztę poupychać po podserwisach odpowiedzialnych za każdy element z danej dziedziny. 