package Lights;

public interface Light {

    void on();

    void off();
}
