package doors;

public interface Door {
    void open();

    void close();

    void lock();

    void unlock();
}
